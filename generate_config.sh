#!/bin/bash -e

sed -e "s;%DB_HOST%;$1;g" -e "s;%LOGIN_HOST%;$2;g" src/main/resources/application.properties.template > src/main/resources/application.properties