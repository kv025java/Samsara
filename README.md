[![build status](https://gitlab.com/kv025java/Samsara/badges/master/build.svg)](https://gitlab.com/kv025java/Samsara/commits/master)
# Samsara

Auth service

## What you'll need
- JDK 1.7+
- Maven 3+

## Stack
- Java
- Spring Boot
- FreeMarker

## Run
`mvn spring-boot:run`